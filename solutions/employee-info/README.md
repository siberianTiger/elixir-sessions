# Employee Info

## Setup

We have an `Employee Info` application to manage employee data.  For each employee the application keeps track of: `employee ID`, `first name`, `last name`, and `salary`.

On startup, it reads employee information from a file.  A sample input file is provided:  `employee_data.txt`

Currently the application implements these functionalities:

1. print employee information
2. add an employee
3. delete an employee
4. quit the application

## Exercise

Your boss has requested the following new behaviours:

1. change the salary of an employee
2. change the first name of an employee

As well, there is part of the program that needs to be filled out before the
final @pending test will pass.

## Extra credit:

This functionality has not been implemented yet:

* save changes back to the employee file on disk

## Execute the tests

Execute the tests with
```elixir
$ elixir employee_info_test.exs
```

## Running the program

You can run the program with these commands:

```elixir
$ iex
iex> c "employee_info.exs"
iex> EmployeeInfo.main("employee_data.txt")
```
