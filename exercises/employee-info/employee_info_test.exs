Code.load_file("employee_info.exs", __DIR__)

ExUnit.start()
ExUnit.configure(exclude: :pending, trace: true)

defmodule EmployeeInfoTest do
  use ExUnit.Case

  setup do
     jane = %Employee{id: 1,
                      first_name: "Jane",
                      last_name: "Doe",
                      salary: 50000}

     john = %Employee{id: 2,
                      first_name: "John",
                      last_name: "Smith",
                      salary: 25000}

     {:ok, employee1: jane, employee2: john}
  end

#  @tag :pending
  test "can add an employee", employees do
    # setup
    jane = employees[:employee1]
    john = employees[:employee2]
    expect_data = [john] ++ [jane]

    # test
    actual_data =
      []
      |> EmployeeInfo.add_employee(jane.first_name, jane.last_name, jane.salary)
      |> EmployeeInfo.add_employee(john.first_name, john.last_name, john.salary)

    # check 1
    assert( length(actual_data) == length(expect_data) )

    # check 2
    actual_jane = EmployeeInfo.get_employee_record(actual_data, jane.id)
    expect_jane = EmployeeInfo.get_employee_record(expect_data, jane.id)
    assert( two_records_match(actual_jane, expect_jane) )

    # check 3
    actual_john = EmployeeInfo.get_employee_record(actual_data, john.id)
    expect_john = EmployeeInfo.get_employee_record(expect_data, john.id)
    assert( two_records_match(actual_john, expect_john) )
  end

#  @tag :pending
  test "can rename an employee's first name", employees do
    # setup
    jane = employees[:employee1]
    frank = %Employee{id: jane.id,
                    first_name: "Frank",
                    last_name: jane.last_name,
                    salary: jane.salary}

    # test
    actual_data = EmployeeInfo.change_first_name([jane], jane.id, "Frank")
    if ! actual_data do
	IO.puts("\n*** EmployeeInfo.change_first_name() returned nil.  Has this been implemented? ***")
	assert( false )
    else
	actual_frank = EmployeeInfo.get_employee_record(actual_data, frank.id)

	# check 1
	assert( length(actual_data) == 1 )

	# check 2
	assert( actual_frank == frank )
	assert( two_records_match(actual_frank, frank) )
    end
  end

#  @tag :pending
  test "can change an employee's salary", employees do
    # setup
    jane = employees[:employee1]
    john = employees[:employee2]

    expect_jane = %Employee{id: jane.id,
                 first_name: jane.first_name,
                 last_name: jane.last_name,
                 salary: 200000}
    expect_john = %Employee{id: john.id,
                 first_name: john.first_name,
                 last_name: john.last_name,
                 salary: 300000}

    # test
    initial_data =
      []
      |> EmployeeInfo.add_employee(jane.first_name, jane.last_name, jane.salary)
      |> EmployeeInfo.add_employee(john.first_name, john.last_name, john.salary)

    # sanity moment: assert checks do not pass yet
    initial_jane = EmployeeInfo.get_employee_record(initial_data, jane.id)
    initial_john = EmployeeInfo.get_employee_record(initial_data, john.id)
    assert( not two_records_match(initial_jane, expect_jane) )
    assert( not two_records_match(initial_john, expect_john) )

    changed_data =
      initial_data
      |> EmployeeInfo.change_salary(jane.id, 200000)
      |> EmployeeInfo.change_salary(john.id, 300000)

    # checks
    changed_jane = EmployeeInfo.get_employee_record(changed_data, jane.first_name, jane.last_name)
    changed_john = EmployeeInfo.get_employee_record(changed_data, john.first_name, john.last_name)

    assert( length(changed_data) == 2 )
    assert( two_records_match(changed_jane, expect_jane) )
    assert( two_records_match(changed_john, expect_john) )
  end

  # We are not comparing the id field; the id might change
  defp two_records_match(record1 = %Employee{}, record2 = %Employee{}) do
    record1.first_name == record2.first_name
      and (record1.last_name  == record2.last_name)
      and (record1.salary     == record2.salary)
  end
end
