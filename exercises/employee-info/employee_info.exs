defmodule Employee do
  defstruct id: 0,
            first_name: "unset",
            last_name: "unset",
            salary: 0

  def to_str(e = %Employee{}) do
    %Employee{id: id, first_name: first, last_name: last, salary: salary} = e
    "#{id}, #{first}, #{last}, #{salary}"
  end
end

defmodule EmployeeInfo do

  # Params
  #  filename: name of employee file that we read from disk
  def main(filename) do
    IO.puts("== Employee Info Application ==")

    employee_data = filename
      |> File.read!()
      |> load_employee_data

    main_loop(employee_data, filename)
  end

  def load_employee_data(lines) do
    lines
    |> String.split("\n", trim: true)
    |> Enum.map(&deconstruct_line(&1))
  end

  # convert csv_line into an Employee struct
  def deconstruct_line(csv_line) do
    line_parts = String.split(csv_line, ",")
    [id, first_name, last_name, salary] = line_parts

    %Employee{
      id:            id |> String.trim |> String.to_integer,
      first_name:    String.trim(first_name),
      last_name:     String.trim(last_name),
      salary:        salary |> String.trim |> String.to_integer
    }
  end

  def main_loop(employee_data, filename) do
    display_options()
    choice = get_choice()
    IO.puts("")

    new_employee_data =
      case choice do
        "1" -> print_employee_data(employee_data)
        "2" -> prompt_and_add_employee(employee_data)
        "3" -> prompt_and_remove_employee(employee_data)
        "4" -> prompt_and_change_salary(employee_data)
        "5" -> prompt_and_change_first_name(employee_data)
        "6" -> save_employee_data(filename, employee_data)
        "7" -> exit_program()
        all -> IO.puts("Unknown selection: #{inspect all}, please try again.")
               employee_data
      end
    main_loop(new_employee_data, filename)
  end

  def display_options do
    IO.puts("\nChoices:")
    IO.puts("  1 - Print employee data")
    IO.puts("  2 - Add employee")
    IO.puts("  3 - Remove employee")
    IO.puts("  4 - Change employee salary")
    IO.puts("  5 - Change employee name")
    IO.puts("  6 - Save employee data")
    IO.puts("  7 - Quit application")
  end

  def get_choice do
    IO.gets("Please enter your choice: ")
    |> String.trim()
  end

  def print_employee_data(employee) do
    IO.inspect(employee, label: "Employee Data")
  end

  def prompt_and_add_employee(employee_data) do
    IO.puts("Add Employee selected")
    first_name = IO.gets("  Enter employee first name: ") |> String.trim()
    last_name = IO.gets("  Enter employee last name: ") |> String.trim()
    salary = IO.gets("  Enter employee salary (integer): ")
             |> String.trim()
             |> String.to_integer()
    add_employee(employee_data, first_name, last_name, salary)
  end

  def prompt_and_remove_employee(employee_data) do
    IO.puts("Remove Employee selected")
    id = IO.gets("  Enter ID of employee to remove: ") |> String.trim() |> String.to_integer()
    case get_employee_record(employee_data, id) do
      nil ->
        print_employee_id_not_found(id)
        employee_data
      employee ->
        remove_employee(employee_data, employee)
    end
  end

  def prompt_and_change_salary(employee_data) do
    IO.puts("Change Salary selected")
    id = IO.gets("  Enter ID of employee to change: ") |> String.trim() |> String.to_integer()
    case get_employee_record(employee_data, id) do
      nil ->
        print_employee_id_not_found(id)
        employee_data
      employee ->
        salary = IO.gets("  Enter NEW employee salary: ") |> String.trim() |> String.to_integer()
        change_salary(employee_data, employee, salary)
    end
  end

  def prompt_and_change_first_name(employee_data) do
    IO.puts("Change First Name selected")
    id = IO.gets("  Enter ID of employee to change: ") |> String.trim() |> String.to_integer()
    case get_employee_record(employee_data, id) do
      nil ->
        print_employee_id_not_found(id)
        employee_data
      employee ->
        first_name = IO.gets("  Enter NEW employee first name: ") |> String.trim()
        change_first_name(employee_data, employee, first_name)
    end
  end

  def print_employee_id_not_found(id) do
    IO.puts("  Could not find an employee with id #{id}.  Please try again.")
  end

  # Use this version when any available ID would be fine
  def add_employee(employee_data, first_name, last_name, salary)
      when is_binary(first_name)
      and is_binary(last_name)
      and is_integer(salary) do

    unique_id = get_unique_id(employee_data)
    add_employee(employee_data, unique_id, first_name, last_name, salary)
  end

  # Use this version when you know the ID you want to use
  def add_employee(employee_data, id, first_name, last_name, salary)
      when is_integer(id)
      and is_binary(first_name)
      and is_binary(last_name)
      and is_integer(salary) do

    new_record = %Employee{
      id: id,
      first_name: first_name,
      last_name: last_name,
      salary: salary
    }
    IO.puts("  Adding record: #{inspect new_record}")
    [new_record | employee_data]
  end

  def remove_employee(employee_data, employee) do
    IO.puts("  Record selected to be deleted: [#{inspect employee}]")
    List.delete(employee_data, employee)
  end

  def change_salary(employee_data, employee, new_salary) when is_integer(new_salary) do
    ## ************************************************************************
    ## TODO: insert code here, return an updated list of employee_data structs.
    ## The employee's ID should not change.
    ## ************************************************************************
  end

  def change_first_name(employee_data, employee, new_name) when is_binary(new_name) do
    ## ************************************************************************
    ## TODO: insert code here, return an updated list of employee_data structs.
    ## The employee's ID should not change.
    ## ************************************************************************
  end

  def get_employee_record(employee_data, id) when is_integer(id) do
    Enum.find(employee_data, fn(element) ->
      match?(%Employee{id: ^id}, element)
    end)
  end

  # This version used by the unit tests
  def get_employee_record(employee_data, first_name, last_name)
      when is_binary(first_name) and is_binary(last_name) do

    Enum.find(employee_data, fn(element) ->
      match?(%Employee{first_name: ^first_name, last_name: ^last_name}, element)
    end)
  end

  def save_employee_data(filename, employee_data) do
    IO.puts("Saving data..")
    {:ok, file} = File.open(filename, [:write])

    employee_data
    |> sort_employee_data()
    |> Enum.each(fn x -> write_single_employee(file, x) end)

    employee_data
  end

  def sort_employee_data(employee_data) when is_list(employee_data)
                                         and length(employee_data) >= 2 do
    employee_data
    |> Enum.sort(fn x, y -> compare_ids(x, y) end)
  end

  def sort_employee_data(employee_data) when is_list(employee_data) do
    employee_data
  end

  def compare_ids(%Employee{id: id1}, %Employee{id: id2}) do
    id1 < id2
  end

  def write_single_employee(file, e = %Employee{}) do
    str = Employee.to_str(e)
    IO.puts("  Writing: #{str}")
    IO.binwrite(file, "#{str}\n")
  end

  def get_unique_id([]), do: 1
  def get_unique_id(employee_data) do
    %Employee{id: max_id} = Enum.max_by(employee_data, fn %Employee{id: id} -> id end)
    max_id + 1
  end

  def exit_program() do
    IO.puts("Goodbye")
    System.halt(0)
  end
end
