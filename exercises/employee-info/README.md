# Employee Info

## Setup

We have an `Employee Info` application to manage employee data.  For each employee the application keeps track of: `employee ID`, `first name`, `last name`, and `salary`.

On startup, it reads employee information from a file.  A sample input file is provided:  `employee_data.txt`

Currently the application implements these functionalities:

1. print the current employees data
2. add an employee
3. delete an employee
4. save the employees data to disk (overwrites previous employees data file)
5. quit the application

## Exercise

Your boss has requested the following new behaviours:

1. change the salary of an employee
2. change the first name of an employee

## Execute the tests

Execute the unit tests with
```elixir
$ elixir employee_info_test.exs
```

To avoid executing a particular test, uncomment the line "@tag :pending"
that comes before the test.  If this line is uncommented, the test will not execute.

## Running the program

You can run the program with these commands:

```elixir
$ iex
iex> c "employee_info.exs"
iex> EmployeeInfo.main("employee_data.txt")
```
