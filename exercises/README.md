
# Elixir Sessions

# Context

I am leading a number of lunch hour sessions at my work, to spread knowledge
about Elixir.

For more details, see [my blog](https://justingamble.github.io/elixir/study/learning/evangelize/2019/05/20/study-session-1.html)

# Exercises:

1. space-age : exercise on functions
2. employee-info : exercise on pattern-matching
