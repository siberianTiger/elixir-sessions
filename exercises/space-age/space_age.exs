defmodule SpaceAge do
  @type planet ::
          :mercury
          | :venus
          | :earth
          | :mars
          | :jupiter
          | :saturn
          | :uranus
          | :neptune
  @earth_seconds_in_year 31557600

  @doc """
  Return the number of years a person that has lived for 'seconds' seconds is
  aged on 'planet'.
  """
  @spec age_on(planet, pos_integer) :: float
  def age_on(planet, seconds) do
    get_earth_years(seconds)
  end

  defp get_earth_years(seconds) do
    seconds / @earth_seconds_in_year
  end

  @doc """
  format_num() is useful for debugging, if you want to use IO.puts/1
  to see the number of years without having exponents or lots of decimals
  involved in output.

  ## Example

    iex> SpaceAge.format_num(1_000_000_000 / 31557600)
    31.69
  """
  defp format_num(float) do
    :erlang.float_to_binary(float, [decimals: 2])
  end
end
